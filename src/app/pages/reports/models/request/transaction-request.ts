export interface TransactionRequest {
  date: string;
  authorization: string;
  cardNumber: string;
}
