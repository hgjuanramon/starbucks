export interface GenerateReportRequest {
  month: string;
  year: string;
  day: string;
}
