export interface Transaction {
  Fecha: string;
  Hora: string;
  Monto: string;
  NumAutorizacionBanco: string;
  NumAutorizacionVlink: string;
  TarjetaRewards: string;
  TarjetaBanco: string;
  RespuestaBanco: string;
  Esreversada: string;
  IdCodigoRespuesta: string;
  Semaforo: string;
}
