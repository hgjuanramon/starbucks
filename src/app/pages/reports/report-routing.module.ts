import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { GenerateTransactionReportComponent } from "./components/generate-transaction-report/generate-transaction-report.component";
import { TransactionReportComponent } from "./components/transaction-report/transaction-report.component";
import { AuthGuard } from "src/app/core/guards/auth.guard";

const starbucksRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "generate-report",
        component: GenerateTransactionReportComponent
      },
      {
        path: "transaction-report",
        component: TransactionReportComponent
      }
    ],
    canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(starbucksRoutes)],
  exports: [RouterModule]
})
export class ReportRoutingModule {}
