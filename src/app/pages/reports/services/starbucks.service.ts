import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { GenerateReportRequest } from "../models/request/generate-report";
import { Observable } from "rxjs";
import { StarbucksConstant } from "../constants/starbucks.constant";
import { TransactionRequest } from "../models/request/transaction-request";
import { TransactionResponse } from "../models/response/transaction-response";
import { Response } from "src/app/core/models/response";

@Injectable({
  providedIn: "root"
})
export class StarbucksService {
  constructor(private httpClient: HttpClient) {}

  public generateTransactionReport = (
    request: GenerateReportRequest
  ): Observable<Response> => {
    return this.httpClient.post<Response>(
      StarbucksConstant.GENERATE_TRANSACTION_REPORT,
      { Anio: request.year, Mes: request.month, Dia: request.day }
    );
  };

  public getAllTransactions = (
    request: TransactionRequest
  ): Observable<TransactionResponse> => {
    return this.httpClient.post<TransactionResponse>(
      StarbucksConstant.GET_ALL_TRANSACTIONS,
      {
        Fecha: request.date,
        Autorizacion: request.authorization,
        Tarjeta: request.cardNumber
      }
    );
  };
}
