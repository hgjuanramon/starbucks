import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  AfterViewInit
} from "@angular/core";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { Transaction } from "../../models/entity/transaction";
import { StarbucksService } from "../../services/starbucks.service";
import { AlertService } from "src/app/core/services/alert.service";
import { DatePipe } from "@angular/common";
import { Title } from "@angular/platform-browser";

@Component({
  selector: "app-transaction-report",
  templateUrl: "./transaction-report.component.html",
  styleUrls: ["./transaction-report.component.scss"],
  providers: [DatePipe]
})
export class TransactionReportComponent implements OnInit, AfterViewInit {
  searchForm: FormGroup;
  items: Transaction[] = [];
  public columns: Object[] = [];
  pageSize = 15;
  spinnerVisible: boolean = false;
  title = "Reporte de Transaciones";
  @ViewChild("ngSearchForm", { static: false }) ngSearchForm;
  constructor(
    private fb: FormBuilder,
    public ref: ChangeDetectorRef,
    private urlService: StarbucksService,
    public alertService: AlertService,
    private datePipe: DatePipe,
    private titleService: Title
  ) {}

  ngOnInit() {
    this.buildForm();
    this.titleService.setTitle(this.title);
  }

  ngAfterViewInit(): void {
    this.columns = this.getColumns();
    this.ref.detectChanges();
  }

  private buildForm(): void {
    this.searchForm = this.fb.group({
      date: [null, [Validators.required]],
      cardNumber: [
        ,
        [
          Validators.minLength(16),
          Validators.maxLength(16),
          Validators.pattern("^-?[0-9]\\d*(\\.\\d{1,2})?$")
        ]
      ],
      authorization: [null]
    });
  }

  public doSearch(request: FormGroup) {
    this.spinnerVisible = true;
    this.alertService.clear();
    request.value.date = this.datePipe.transform(
      new Date(request.value.date),
      "MM/dd/yyyy"
    );
    this.urlService.getAllTransactions(request.value).subscribe(
      response => {
        this.spinnerVisible = false;
        if (response.Respuesta.CodigoRespuesta == 0) {
          this.items = response.Tx;
        } else {
          this.items = [];
          this.alertService.success(response.Respuesta.Mensaje);
        }
      },
      error => {
        this.spinnerVisible = false;
        console.log(error);
      }
    );
  }

  private getColumns(): Object[] {
    return [
      {
        name: "Fecha",
        prop: "Fecha",
        flexGrow: 0.5
      },
      {
        name: "Hora",
        prop: "Hora",
        flexGrow: 0.5
      },
      {
        name: "Monto",
        prop: "Monto",
        flexGrow: 0.5
      },
      {
        name: "Autorización Banco",
        prop: "NumAutorizacionBanco",
        flexGrow: 0.5
      },
      {
        name: "Autorización VLINK",
        prop: "NumAutorizacionVlink",
        flexGrow: 0.5
      },
      {
        name: "Tarjeta Rewards",
        prop: "TarjetaRewards",
        flexGrow: 1
      },
      {
        name: "Tarjeta Banco",
        prop: "TarjetaBanco",
        flexGrow: 1
      },
      {
        name: "Respuesta Banco",
        prop: "RespuestaBanco",
        flexGrow: 1
      },
      {
        name: "#",
        prop: "Semaforo",
        flexGrow: 0.5
      }
    ];
  }

  public getCellClass({ row, column, value }) {
    //debugger;
    let className = "";

    if (column.name == "#") {
      if (row.Esreversada == 0 && row.IdCodigoRespuesta == 0) {
        className = " default-text";
      }

      if (row.Esreversada == 1 && row.IdCodigoRespuesta == 0) {
        className = " blue-text";
      }

      if (row.Esreversada == 0 && row.IdCodigoRespuesta > 0) {
        className = " red-text";
      }

      if (row.Esreversada > 0 && row.IdCodigoRespuesta > 0) {
        className = " orange-text";
      }
    }

    return className;
  }

  public doReset() {
    this.items = [];
    this.ngSearchForm.resetForm();
  }
}
