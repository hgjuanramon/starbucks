import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  ChangeDetectorRef,
  OnChanges
} from "@angular/core";
import { StarbucksService } from "../../services/starbucks.service";
import { Title } from "@angular/platform-browser";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { StarbucksMessageConstant } from "../../constants/starbucks-message.constant";
import { DateHelper } from "src/app/core/helpers/date-helper";
import { AlertService } from "src/app/core/services/alert.service";
import { MatSelectChange } from "@angular/material";

@Component({
  selector: "app-generate-transaction-report",
  templateUrl: "./generate-transaction-report.component.html",
  styleUrls: ["./generate-transaction-report.component.scss"]
})
export class GenerateTransactionReportComponent
  implements OnInit, AfterViewInit {
  title = "Generar Reporte de Transaciones";
  searchForm: FormGroup;
  public yearList: any[] = [];
  public monthList: any[] = [];
  public dayList: any[] = [];
  spinnerVisible: boolean = false;
  @ViewChild("ngSearchForm", { static: false }) ngSearchForm;
  constructor(
    private fb: FormBuilder,
    private urlService: StarbucksService,
    private dateHelper: DateHelper,
    public alertService: AlertService,
    private titleService: Title,
    private ref: ChangeDetectorRef
  ) {
    this.yearList = this.dateHelper.getYears();
    this.monthList = this.dateHelper.getMonths();
  }

  ngOnInit() {
    this.buildForm();
    this.titleService.setTitle(this.title);
    this.setDefaultSearchForm();
  }

  ngAfterViewInit() {
    this.ref.detectChanges();
  }

  private buildForm() {
    this.searchForm = this.fb.group({
      year: ["", [Validators.required]],
      month: ["", [Validators.required]],
      day: [""]
    });
  }

  get year() {
    return this.searchForm.get("year") as FormControl;
  }

  get month() {
    return this.searchForm.get("month") as FormControl;
  }

  get day() {
    return this.searchForm.get("day") as FormControl;
  }

  private setDefaultSearchForm() {
    this.searchForm.get("year").setValue(new Date().getFullYear());
  }

  public doGenerateReport(): void {
    this.alertService.clear();
    this.spinnerVisible = true;
    this.urlService.generateTransactionReport(this.searchForm.value).subscribe(
      response => {
        this.spinnerVisible = false;
        if (response.CodigoRespuesta != 0) {
          this.alertService.info(response.Mensaje);
        } else {
          this.alertService.info(
            StarbucksMessageConstant.MESSAGE_SUCCES_REPORT
          );
        }
      },
      error => {
        this.spinnerVisible = false;
        console.log(error);
      }
    );
  }

  public onChangeMonth(ev: MatSelectChange) {
    if (this.month.value) {
      this.dayList = this.dateHelper.getDays(this.year.value, this.month.value);
    } else {
      this.dayList = [];
    }
  }

  public doReset() {
    this.alertService.clear();
    this.ngSearchForm.resetForm();
    this.setDefaultSearchForm();
  }
}
