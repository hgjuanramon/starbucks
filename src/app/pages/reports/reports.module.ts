import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "src/app/shared/shared.module";
import { MaterialModule } from "src/app/material.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ReportRoutingModule } from "./report-routing.module";
import { TransactionReportComponent } from "./components/transaction-report/transaction-report.component";
import { GenerateTransactionReportComponent } from "./components/generate-transaction-report/generate-transaction-report.component";

@NgModule({
  declarations: [
    TransactionReportComponent,
    GenerateTransactionReportComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ReportRoutingModule
  ]
})
export class ReportsModule {}
