import { Injectable } from "@angular/core";
import { IAuthStatus } from "../models/iauth-status";
import {
  Observable,
  BehaviorSubject,
  throwError as observableThrowError
} from "rxjs";
import { map, catchError } from "rxjs/operators";
import { CacheService } from "src/app/core/services/cache.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { transformError } from "src/app/core/commons/common";
import { AuthApiConstant } from "../constants/auth-api.constant";
import { Menu } from "src/app/core/models/menu";
import { IServerAuthResponse } from "../models/iserver-auth-response";

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json"
  })
};

@Injectable({
  providedIn: "root"
})
export class AuthService extends CacheService {
  private readonly authProvider: (
    email: string,
    password: string
  ) => Observable<IServerAuthResponse>;
  authStatus = new BehaviorSubject<IAuthStatus>(
    this.getItem("authStatus") || defaultAuthStatus
  );

  constructor(private httpClient: HttpClient) {
    super();
    this.authStatus.subscribe(authStatus => {
      this.setItem("authStatus", authStatus);
    });
    this.authProvider = this.userAuthProvider;
  }

  private userAuthProvider(
    email: string,
    password: string
  ): Observable<IServerAuthResponse> {
    return this.httpClient.post<IServerAuthResponse>(
      AuthApiConstant.LOGIN_ENDPOINT,
      { correo: email, password: password },
      httpOptions
    );
  }

  login(email: string, password: string): Observable<IAuthStatus> {
    this.logout();
    const loginResponse = this.authProvider(email, password).pipe(
      map(value => {
        if (value.Respuesta.CodigoRespuesta == 0) {
          this.setToken(value.UsuarioPos.NombreUsuario);
        }
        return value as IAuthStatus;
      }),
      catchError(transformError)
    );

    loginResponse.subscribe(
      res => {
        this.authStatus.next(res);
      },
      err => {
        this.logout();
        return observableThrowError(err);
      }
    );

    return loginResponse;
  }

  public getMenus = (): Menu[] => {
    return [
      {
        Rol: "Starbucks",
        Menu: "Generar Reporte Starbucks",
        Url: "/reports/generate-report",
        Id_Url: 1
      },
      {
        Rol: "Starbucks",
        Menu: "Consulta Transaccion,",
        Url: "/reports/transaction-report",
        Id_Url: 2
      }
    ];
  };

  logout() {
    this.clearToken();
    this.authStatus.next(defaultAuthStatus);
  }

  private setToken(jwt: string) {
    this.setItem("jwt", jwt);
  }

  getToken(): string {
    return this.getItem("jwt") || "";
  }

  private clearToken() {
    this.removeItem("jwt");
  }

  getAuthStatus(): IAuthStatus {
    return this.getItem("authStatus");
  }
}
const defaultAuthStatus: IAuthStatus = { Respuesta: null, UsuarioPos: null };
