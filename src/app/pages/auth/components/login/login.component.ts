import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import { Title } from "@angular/platform-browser";

interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  title = "Login";
  loginError = "";
  loginForm: FormGroup;
  submitted: boolean = false;
  isVisible: boolean = false;
  hidePassword = true;
  loading = false;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private titleService: Title
  ) {}

  ngOnInit() {
    this.authService.logout();
    this.buildLoginForm();
    this.titleService.setTitle(this.title);
  }

  buildLoginForm(): void {
    this.loginForm = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: [
        "",
        [Validators.required, Validators.minLength(2), Validators.maxLength(20)]
      ]
    });
  }

  login(submittedForm: FormGroup) {
    //this.isVisible = true;
    if (submittedForm.valid) {
      this.loading = true;
      this.authService
        .login(submittedForm.value.email, submittedForm.value.password)
        .subscribe(
          authResponse => {
            this.loading = false;
            //this.isVisible = false;
            if (authResponse.Respuesta.CodigoRespuesta == 0) {
              this.router.navigate(["/dashboard"]);
            } else {
              this.loginError = authResponse.Respuesta.Mensaje;
            }
          },
          error => {
            this.loading = false;
            //this.isVisible = false;
            this.loginError = error;
          }
        );
    }
  }
}
