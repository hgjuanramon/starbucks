export interface Rol {
  IdRol: number;
  Nombre: string;
  Descripcion: string;
}
