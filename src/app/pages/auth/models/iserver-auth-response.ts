import { User } from "./user";
import { Response } from "src/app/core/models/response";

export interface IServerAuthResponse {
  Respuesta: Response;
  UsuarioPos: User;
}
