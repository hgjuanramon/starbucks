import { User } from "./user";
import { Response } from "src/app/core/models/response";

export interface IAuthStatus {
  Respuesta: Response;
  UsuarioPos: User;
}
