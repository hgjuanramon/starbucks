import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { AuthService } from "src/app/pages/auth/services/auth.service";
import { Navbar } from "src/app/core/models/navbar";
import { Menu } from "src/app/core/models/menu";

@Component({
  selector: "app-vertical",
  templateUrl: "./vertical.component.html",
  styleUrls: ["./vertical.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class VerticalComponent implements OnInit {
  navItems: Navbar[];
  username: string = "Usuario";
  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.buildListMenu();
    this.username = this.authService.getToken();
  }

  buildListMenu = (): void => {
    this.navItems = this._buildMenus(this.authService.getMenus()) as Navbar[];
  };

  _buildMenus = (records: Menu[]) => {
    var myArray = [];
    if (records) {
      var items = records.reduce(function(obj, item) {
        obj[item.Rol] = obj[item.Rol] || [];
        obj[item.Rol].push({
          displayName: item.Menu,
          route: item.Url,
          iconName: "arrow_right"
        });
        return obj;
      }, {});

      myArray = Object.keys(items).map(function(key) {
        return {
          displayName: key,
          iconName: "more_vert",
          children: items[key]
        };
      });
    }

    return myArray;
  };
}
