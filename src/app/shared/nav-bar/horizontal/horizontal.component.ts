import { Component, OnInit, Input, ViewChild } from "@angular/core";
import { Navbar } from "src/app/core/models/navbar";
import { Router } from "@angular/router";

@Component({
  selector: "app-horizontal",
  templateUrl: "./horizontal.component.html",
  styleUrls: ["./horizontal.component.scss"]
})
export class HorizontalComponent implements OnInit {
  @Input() items: Navbar[];
  @ViewChild("childMenu", { static: true }) public childMenu: any;

  constructor(public router: Router) {}

  ngOnInit() {}
}
