import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TableViewComponent } from "./table-view/table-view.component";
import { SpinnerComponent } from "./spinner/spinner.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { AdminLayoutComponent } from "./layout/admin-layout/admin-layout.component";
import { AuthLayoutComponent } from "./layout/auth-layout/auth-layout.component";
import { AlertComponent } from "./alert/alert.component";
import { HorizontalComponent } from "./nav-bar/horizontal/horizontal.component";
import { VerticalComponent } from "./nav-bar/vertical/vertical.component";
import { RouterModule } from "@angular/router";
import { MaterialModule } from "../material.module";
import { FlexLayoutModule } from "@angular/flex-layout";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";

@NgModule({
  declarations: [
    TableViewComponent,
    SpinnerComponent,
    NotFoundComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    AlertComponent,
    VerticalComponent,
    HorizontalComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgxDatatableModule,
    MaterialModule,
    FlexLayoutModule
  ],
  exports: [
    TableViewComponent,
    SpinnerComponent,
    NotFoundComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    AlertComponent,
    VerticalComponent,
    HorizontalComponent
  ]
})
export class SharedModule {}
