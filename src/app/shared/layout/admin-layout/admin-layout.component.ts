import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";
import { Idle, DEFAULT_INTERRUPTSOURCES } from "@ng-idle/core";
import { Keepalive } from "@ng-idle/keepalive";
import { AuthService } from "src/app/pages/auth/services/auth.service";

declare var $: any;

@Component({
  selector: "app-admin-layout",
  templateUrl: "./admin-layout.component.html",
  styleUrls: ["./admin-layout.component.scss"]
})
export class AdminLayoutComponent implements OnInit {
  title = "Starbucks web";
  _displayLogin = false;
  idleState = "Not started.";
  timedOut = false;
  lastPing?: Date = null;
  constructor(
    private router: Router,
    private authService: AuthService,
    private idle: Idle,
    private keepalive: Keepalive
  ) {
    // sets an idle timeout of 5 seconds, for testing purposes.
    idle.setIdle(environment.timeOut);
    // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
    idle.setTimeout(10);
    // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    idle.onIdleEnd.subscribe(() => {
      this.reset();
    });

    idle.onTimeout.subscribe(() => {
      $("#idleModal").modal("hide");
      this.idleState = "Sesion Terminada.";
      this.timedOut = true;
      this.authService.logout();
      this.router.navigate(["/"]);
    });

    idle.onIdleStart.subscribe(() => {
      $("#idleModal").modal({ backdrop: false });
    });

    idle.onTimeoutWarning.subscribe(countdown => {
      this.idleState = "La Página se Cerrará en " + countdown + " Segundos.";
    });

    // sets the ping interval to 15 seconds
    keepalive.interval(15);

    this.reset();
  }

  ngOnInit(): void {
    this.authService.authStatus.subscribe(authstatus => {
      const jwt = this.authService.getToken();
      setTimeout(
        () => ((this._displayLogin = !(jwt == null || jwt === "")), 0)
      );
    });
  }

  get displayMenu() {
    return this._displayLogin;
  }

  reset() {
    this.idle.watch();
    this.timedOut = false;
  }

  logout() {
    $("#idleModal").modal("hide");
    this.authService.logout();
    this.router.navigate(["/"]);
  }

  stay() {
    $("#idleModal").modal("hide");
    this.reset();
  }
}
