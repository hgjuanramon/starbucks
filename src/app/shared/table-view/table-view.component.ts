import {
  Component,
  OnInit,
  Input,
  TemplateRef,
  EventEmitter,
  ViewChild,
  Output
} from "@angular/core";

@Component({
  selector: "app-table-view",
  templateUrl: "./table-view.component.html",
  styleUrls: ["./table-view.component.scss"]
})
export class TableViewComponent implements OnInit {
  @Input() minTableHeight: number = 500;
  @Input() items: object[] = [];
  @Input() columns: object[] = [];
  @Input() limit?: number;
  @Input() count?: number;
  @Input() offset?: number;
  @Input() detailTemplate: TemplateRef<any>;
  @Input() select;
  @Input() cellClass;
  selected = [];
  @Output() selectEvent = new EventEmitter<object[]>();
  @ViewChild("myTable", { static: false }) table: any;

  constructor() {}

  ngOnInit() {}

  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    this.selectEvent.emit(this.selected);
  }
}
