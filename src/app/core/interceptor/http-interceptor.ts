import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent
} from "@angular/common/http";
import { Router } from "@angular/router";
import { Observable, throwError as observableThrowError } from "rxjs";
import { catchError } from "rxjs/operators";
import { AuthService } from "src/app/pages/auth/services/auth.service";

@Injectable()
export class AuthHttpInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    //const jwt = this.authService.getToken();
    const authRequest = req.clone({ setHeaders: {} });

    return next.handle(authRequest).pipe(
      catchError((err, caught) => {
        if (err.status === 401) {
          this.router.navigate(["/login"], {
            queryParams: { redirectUrl: this.router.routerState.snapshot.url }
          });
        }
        return observableThrowError(err);
      })
    );
  }
}
