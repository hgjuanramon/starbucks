export interface Navbar {
  displayName: string;
  disabled?: boolean;
  iconName: string;
  route?: string;
  children?: Navbar[];
}
