import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  CanActivate,
  CanActivateChild,
  CanLoad,
  Router,
  Route
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "src/app/pages/auth/services/auth.service";
import { IAuthStatus } from "src/app/pages/auth/models/iauth-status";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  protected currentAuthStatus: IAuthStatus;

  constructor(private authService: AuthService, private router: Router) {
    this.authService.authStatus.subscribe(
      authStatus => (this.currentAuthStatus = this.authService.getAuthStatus())
    );
  }

  canLoad(route: Route): boolean | Observable<boolean> | Promise<boolean> {
    return this.chechLogin();
  }
  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    return this.checkPermissions(childRoute);
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkPermissions(next);
  }

  protected chechLogin() {
    if (
      this.authService.getToken() == null ||
      this.authService.getToken() === ""
    ) {
      //alert("No esta logueado.")
      this.router.navigate(["/login"]);
      return false;
    }

    return true;
  }

  protected checkPermissions(route?: ActivatedRouteSnapshot) {
    let roleMatch = true;
    if (route) {
      const expectedRole = route.data.expectedRole;
      if (expectedRole) {
        //roleMatch = this.currentAuthStatus === expectedRole;
      }
    }
    if (!roleMatch) {
      alert("No tiene permiso para ver este modulo");
      this.router.navigate(["home"]);
      return false;
    }

    return true;
  }
}
