import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class DateHelper {
  getYears(init: number = 2015, end: number = 0) {
    const years = [];
    end = end == 0 ? new Date().getFullYear() + 1 : end;
    for (let i = init; i < end; i++) {
      years.push({ id: i, value: i });
    }

    return years.reverse();
  }

  public getMonths() {
    const months = [
      {
        id: 1,
        value: "Enero"
      },
      {
        id: 2,
        value: "Febrero"
      },
      {
        id: 3,
        value: "Marzo"
      },
      {
        id: 4,
        value: "Abril"
      },
      {
        id: 5,
        value: "Mayo"
      },
      {
        id: 6,
        value: "Junio"
      },
      {
        id: 7,
        value: "Julio"
      },
      {
        id: 8,
        value: "Agosto"
      },
      {
        id: 9,
        value: "Septiembre"
      },
      {
        id: 10,
        value: "Octubre"
      },
      {
        id: 11,
        value: "Noviembre"
      },
      {
        id: 12,
        value: "Diciembre"
      }
    ];

    return months;
  }

  public getDays(year: number, month: number) {
    const days = [];
    year = year ? year : new Date().getFullYear();
    month = month ? month : new Date().getMonth() + 1;
    const numberDays = this.daysInMonth(year, month);

    if (year && month) {
      for (let i = 1; i <= numberDays; i++) {
        days.push({ id: i, value: i });
      }
    }

    return days;
  }

  daysInMonth(year, month) {
    return new Date(year, month, 0).getDate();
  }
}
