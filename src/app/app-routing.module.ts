import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DashboardComponent } from "./pages/dashboard/dashboard.component";
import { AuthGuard } from "./core/guards/auth.guard";
import { AdminLayoutComponent } from "./shared/layout/admin-layout/admin-layout.component";
import { AuthLayoutComponent } from "./shared/layout/auth-layout/auth-layout.component";
import { LoginComponent } from "./pages/auth/components/login/login.component";
import { LogoutComponent } from "./pages/auth/components/logout/logout.component";
import { NotFoundComponent } from "./shared/not-found/not-found.component";

const routes: Routes = [
  {
    path: "dashboard",
    component: AdminLayoutComponent,
    children: [
      {
        path: "",
        component: DashboardComponent
      }
    ],
    canLoad: [AuthGuard]
  },
  {
    path: "reports",
    component: AdminLayoutComponent,
    loadChildren: () =>
      import("./pages/reports/reports.module").then(m => m.ReportsModule),
    canLoad: [AuthGuard]
  },
  {
    path: "",
    redirectTo: "login",
    pathMatch: "full"
  },
  {
    path: "login",
    component: AuthLayoutComponent,
    children: [
      {
        path: "",
        component: LoginComponent
      }
    ]
  },
  {
    path: "logout",
    component: LogoutComponent
  },
  {
    path: "**",
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
